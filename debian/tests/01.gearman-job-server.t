#!/bin/sh

test_description="Check that the gearman-job-server starts"

. /usr/share/sharness/sharness.sh

test_expect_success "service status" "
  /usr/sbin/service gearman-job-server status
"

test_expect_success "process running" "
  pgrep gearmand
"

test_done
